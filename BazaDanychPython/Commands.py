from  MainFile import  Operations
import hashlib
from UpdateOptions import Updates
Options=Operations()
UpdateOptions=Updates()
Number = int(input("Wybierz opcje:\n1-Dodaj rekord\n2-Usun rekord\n3-Zmien rekord\n4-Wyswietl tabele\n5-Wyjdz\n"))
if Number<1 or Number>5:
    print("Zła liczba")
    Number = int(input("Wybierz opcje:\n1-Dodaj rekord\n2-Usun rekord\n3-Zmien rekord\n4-Wyswietl tabele\n5-Wyjdz"))
#Add record
elif Number==1:
    Password = str(input("Podaj haslo:"))
    PasswordHide = hashlib.sha3_256()
    PasswordHide.update(Password.encode())
    NewPassword = PasswordHide.hexdigest()
    Options.Add_record(str(input("Imie: ")),str(input("Nazwisko: ")),str(input('Rok.miesiac.dzien urodzenia: ')),str(input('Email:')),str(input('Zainteresowania: ')),str(NewPassword))
#Delete record by give ID
elif Number==2:
    Options.Delete_record(str(input('Podaj ID:')))
#Update recorn
elif Number==3:
    Change=int(input("------------------\nCo chcesz zmienic?\n------------------\n1-Imie\n2-Nazwisko\n3-Date urodzenia\n4-Email\n5-Zainteresowania\n6-Haslo\n"))
    if Change==1:
        UpdateOptions.Update_recordsName(str(input("Podaj nowe Imie: ")),str(input("Podaj Id kolumny ktora chcesz zmienic: ")))
    elif Change==2:
        UpdateOptions.Update_recordsLast_Name(str(input("Podaj nowe Nazwisko: ")),str(input("Podaj Id kolumny ktora chcesz zmienic: ")))
    elif Change==3:
        UpdateOptions.Update_recordsDate_Of_Birth(str(input("Podaj nowa date: ")),str(input("Podaj Id kolumny ktora chcesz zmienic: ")))
    elif Change==4:
        UpdateOptions.Update_recordsEmail(str(input("Podaj nowy Email: ")),str(input("Podaj Id kolumny ktora chcesz zmienic: ")))
    elif Change==5:
        UpdateOptions.Update_recordsInterest(str(input("Podaj nowe Zainteresowanie: ")),str(input("Podaj Id kolumny ktora chcesz zmienic: ")))
    elif Change==6:
        Password = str(input("Podaj nowe haslo:"))
        PasswordHide = hashlib.sha3_256()
        PasswordHide.update(Password.encode())
        NewPassword = PasswordHide.hexdigest()
        UpdateOptions.Update_recordsPassw(str(NewPassword),str(input("Podaj Id kolumny która chcesz zmienić: ")))
#Show records
elif Number==4:
    Options.Show_records()
else:
    exit()
