import sqlite3

connect = sqlite3.connect('Users.db')

cursor = connect.cursor()

try:
    cursor.execute(
        "CREATE TABLE UsersInfo (Name TEXT, Last_name TEXT,Date_Of_Birth DATE,Email TEXT,Interests TEXT,Password TEXT)")
    connect.commit()
    print("-------------------------------\nUtworzono Baze Danych UsersInfo\n-------------------------------")
except:
    print("--------------------------\nPolaczona z baza UsersInfo\n--------------------------")
class Operations():
    def Add_record(self,name,last_name, date_of_birth ,email,interests,Newpassword):
        try:
            cursor.execute("INSERT INTO UsersInfo VALUES (?,?,?,?,?,?)",(name,last_name,date_of_birth,email,interests,Newpassword))
            print("-------------------\nUkonczono pomyslenie\n-------------------")
        except:
            print("-----------------------\nNiepowodzenie operacjii\n-----------------------")
        connect.commit()
        connect.close()
    def Delete_record(self,rowId):
        try:
            cursor.execute("DELETE FROM UsersInfo WHERE rowid=(?)",rowId)
            print("-------------------\nUkonczono pomyslenie\n-------------------")
        except:
            print("-----------------------\nNiepowodzenie operacjii\n-----------------------")
        connect.commit()
        connect.close()
    def Show_records(self):
        cursor.execute("SELECT rowid,* FROM UsersInfo")
        elements=cursor.fetchall()
        ####### Visual appearance
        print("ID"+"\tIMIE"+"\t\tNAZWISKO"+"\tDATA URODZENIA"+"\t\tEMAIL"+"\t\t\tZainteresowania"+"\t\t\tHASLO")
        print("----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------")
        for element in elements :
            print(str(element[0])+"\t"+str(element[1])+"\t"+str(element[2])+"\t"+str(element[3])+"\t\t"+str(element[4])+"\t"+str(element[5])+"\t\t\t\t"+str(element[6]))    #"\t" is TAB
        connect.commit()
        connect.close()