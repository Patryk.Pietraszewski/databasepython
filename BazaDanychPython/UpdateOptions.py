from MainFile import cursor,connect
#Update record from give id tabel
class Updates():
    #Update record from give id tabel
    def Update_recordsName(self,NewName,rowId):
        try:
            cursor.execute("UPDATE UsersInfo SET Name=? WHERE rowid=? ",(NewName,rowId))
            print("-------------------\nUkonczono pomyslenie\n-------------------")
        except:
            print("-----------------------\nNiepowodzenie operacjii\n-----------------------")
        connect.commit()
        connect.close()
    def Update_recordsLast_Name(self,NewLastName,rowId):
        try:
            cursor.execute("UPDATE UsersInfo SET Last_name=? WHERE rowid=? ",(NewLastName,rowId))
            print("-------------------\nUkonczono pomyslenie\n-------------------")
        except:
            print("-----------------------\nNiepowodzenie operacjii\n-----------------------")
        connect.commit()
        connect.close()
    def Update_recordsDate_Of_Birth(self,New_Date_Of_Birth,rowId):
        try:
            cursor.execute("UPDATE UsersInfo SET Date_Of_Birth=? WHERE rowid=? ",(New_Date_Of_Birth,rowId))
            print("-------------------\nUkonczono pomyslenie\n-------------------")
        except:
            print("-----------------------\nNiepowodzenie operacjii\n-----------------------")
        connect.commit()
        connect.close()
    def Update_recordsEmail(self,NewEmail,rowId):
        try:
            cursor.execute("UPDATE UsersInfo SET Email=? WHERE rowid=? ",(NewEmail,rowId))
            print("-------------------\nUkonczono pomyslenie\n-------------------")
        except:
            print("-----------------------\nNiepowodzenie operacjii\n-----------------------")
        connect.commit()
        connect.close()
    def Update_recordsInterest(self,NewInterests,rowId):
        try:
            cursor.execute("UPDATE UsersInfo SET Interests=? WHERE rowid=? ",(NewInterests,rowId))
            print("-------------------\nUkonczono pomyslenie\n-------------------")
        except:
            print("-----------------------\nNiepowodzenie operacjii\n-----------------------")  
        connect.commit()
        connect.close()
    def Update_recordsPassw(self,NewPassw,rowId):
        try:
            cursor.execute("UPDATE UsersInfo SET Password=? WHERE rowid=? ",(NewPassw,rowId))
            print("-------------------\nUkonczono pomyslenie\n-------------------")
        except:
            print("-----------------------\nNiepowodzenie operacjii\n-----------------------")  
        connect.commit()
        connect.close()